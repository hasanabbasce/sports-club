//
//  AppDelegate.swift
//  Sportclub
//
//  Created by Hasan Abbas on 26/12/2017.
//  Copyright © 2017 Hasan Abbas. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import FirebaseMessaging
import UserNotifications
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , MessagingDelegate ,UNUserNotificationCenterDelegate , CLLocationManagerDelegate  {

    var window: UIWindow?
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var netService = NetworkingService()
    var locationhere : Bool = false
    
    
    var locality : String?
    var postalCode : String?
    var administrativeArea : String?
    var country : String?
    var countrycode : String?
    



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        FirebaseApp.configure()
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                
            case  .notDetermined, .restricted:
                print("notDetermined restricted")
                
            case  .denied:
                print("No access")
                DispatchQueue.main.async {
                    
                    
                    let alert = UIAlertController(title: "Location Disabled", message: "Please enable your location sharing.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    
                    // show the alert
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            }
                        case .cancel:
                            print("cancel")
                        case .destructive:
                            print("destructive")
                        }}))
                    var cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alert.addAction(cancelAction)
                    
                }
            case .authorizedAlways, .authorizedWhenInUse:
                print("access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
        
        return true
    }
    
    
    func takeToHome(){
        if Auth.auth().currentUser != nil {
            Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
                if(token == nil)
                {
                    self.netService.logOut {}
                }
                else
                {
                    
                
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let homeVC = storyboard.instantiateViewController(withIdentifier: "MainTabController") as! MainTabController
//                    self.window?.rootViewController = homeVC
                }
            })
            
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestLocation()
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            print("denied")
            // show the alert
            
            // Permission denied, do something else
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (locations.first != nil && locationhere == false )  {
            currentLocation = locations[0] as CLLocation
            locationhere = true
            print(currentLocation?.coordinate.latitude)
            print(currentLocation?.coordinate.longitude)
            locationManager.stopUpdatingLocation()
            CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
                
                if (error != nil) {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    self.displayLocationInfo(pm)
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
    }
    
    func displayLocationInfo(_ placemark: CLPlacemark?) {
        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            self.locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            self.postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            self.administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            self.country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            self.countrycode = (containsPlacemark.isoCountryCode != nil) ? containsPlacemark.isoCountryCode : ""
            print(" Postal Code \(self.postalCode)")
            print(" administrativeArea \(self.administrativeArea)")
            print(" country \(self.country)")
            print(" countrycode \(self.countrycode)")
            print(" locality \(self.locality)")
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
        locationManager.stopUpdatingLocation()
    }


}

