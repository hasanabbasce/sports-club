//
//  LeagueViewController.swift
//  Sportclub
//
//  Created by Hasan Abbas on 02/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class LeagueViewController: UIViewController , UITableViewDataSource,UITableViewDelegate , communicateDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        tableview.register(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTableViewCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        myLabel.center = CGPoint(x: self.view.frame.width/2, y: 20)
        myLabel.textAlignment = .center
        if(section == 0)
        {
            myLabel.text = "Created Leagues"
        }
        else
        {
            myLabel.text = "Joined Leagues"
        }
        
        myLabel.textColor = UIColor.white
        vw.addSubview(myLabel)
        vw.backgroundColor = hexStringToUIColor(hex: "#D6B05E")
        // myLabel.center = CGPoint(x: vw., y: 0)
        return vw
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        // cell.delegate = self
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
