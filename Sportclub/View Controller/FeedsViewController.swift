//
//  FeedsViewController.swift
//  Sportclub
//
//  Created by Hasan Abbas on 01/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FeedsViewController: UIViewController , UITableViewDataSource,UITableViewDelegate , communicateDelegate ,NVActivityIndicatorViewable , UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var createFeed: CreateFeed!
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var tableheight: NSLayoutConstraint!
    
    @IBOutlet weak var feedwidgetheight: NSLayoutConstraint!
    
    
    
    let activityData = ActivityData()
    
    var isprofilepicchanged:Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableheight.constant = CGFloat( 180 * 3 )
        tableview.dataSource = self
        tableview.delegate = self
        tableview.register(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTableViewCell")
        
        createFeed.delegate = self

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
       // cell.delegate = self
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tapbtn(sender: Any?, Cell: UITableViewCell?, identifier: String?, Data: Any?) {
        if(identifier == "sendfeed" )
        {
            
        }
        if(identifier == "addphoto" )
        {
            chooseimage()
        }
    
    }

}


extension FeedsViewController
{
    func chooseimage() {
        
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.modalPresentationStyle = .popover
        pickerController.popoverPresentationController?.delegate = self
        pickerController.popoverPresentationController?.sourceView = self.createFeed
        let alertController = UIAlertController(title: "Add a Picture", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Photos Album", style: .default) { (action) in
            pickerController.sourceType = .savedPhotosAlbum
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            //  popoverController.sourceView = self.view
            //  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            //  popoverController.permittedArrowDirections = []
            popoverController.sourceView = self.createFeed
            popoverController.sourceRect = (self.createFeed as AnyObject).bounds;
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            isprofilepicchanged = true
            self.createFeed.postimage.image = imageOrientation(chosenImage)
            feedwidgetheight.constant =  120 + (self.createFeed.postimage.frame.height)
            //self.userpicture.image = imageOrientation(chosenImage)
        }
        self.dismiss(animated: true, completion: nil)
        
       
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
     }


}
