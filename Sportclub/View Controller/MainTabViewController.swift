//
//  MainTabViewController.swift
//  Sportclub
//
//  Created by Hasan Abbas on 01/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavBar()
        // Do any additional setup after loading the view.
    }
    
    
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for i in 0...4 {
            tabBar.items?[i].tag = i
        }
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        //Code to run when a certain tab is selected
        setupNavBar()
    }
    
    
    func setupNavBar()
    {
       if(tabBar.selectedItem?.tag == 1)
       {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.addLeague(sender:)))
        let iconsnotification = UIImage(named: "icons-add")
        navigationItem.rightBarButtonItem?.image = iconsnotification
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.openSearchLeague(sender:)))
        let iconssearch = UIImage(named: "icons-search")
        navigationItem.leftBarButtonItem?.image = iconssearch
        
        navigationItem.title = "League"
       }
       else if(tabBar.selectedItem?.tag == 2)
       {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.addTeam(sender:)))
        let iconsnotification = UIImage(named: "icons-add")
        navigationItem.rightBarButtonItem?.image = iconsnotification
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.openSearchTeam(sender:)))
        let iconssearch = UIImage(named: "icons-search")
        navigationItem.leftBarButtonItem?.image = iconssearch
        
        navigationItem.title = "Team"
       }
       else if(tabBar.selectedItem?.tag == 3)
       {
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "noti", style: .plain, target: self, action: #selector(self.addTapped(sender:)))
//        let iconsnotification = UIImage(named: "icons-notification")
//        navigationItem.rightBarButtonItem?.image = iconsnotification
        navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.openSearch(sender:)))
        let iconssearch = UIImage(named: "icons-search")
        navigationItem.leftBarButtonItem?.image = iconssearch
        
        navigationItem.title = "Inbox"
       }
       else if(tabBar.selectedItem?.tag == 4)
       {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(self.openUpdateProfile(sender:)))
        let iconssettings = UIImage(named: "icons-settings")
        navigationItem.rightBarButtonItem?.image = iconssettings
//
//        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.addTapped(sender:)))
//        let iconssearch = UIImage(named: "icons-search")
//        navigationItem.leftBarButtonItem?.image = iconssearch
        
       // navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem = nil
        navigationItem.title = "Account"
       }
       else
       {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "noti", style: .plain, target: self, action: #selector(self.openNotification(sender:)))
        let iconsnotification = UIImage(named: "icons-notification")
        navigationItem.rightBarButtonItem?.image = iconsnotification
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.openSearch(sender:)))
        let iconssearch = UIImage(named: "icons-search")
        navigationItem.leftBarButtonItem?.image = iconssearch
        
        navigationItem.title = "Feeds"
       }
    }
    
    @objc func addLeague(sender: UIBarButtonItem) {
        if let createLeagueViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateLeagueViewController") as? CreateLeagueViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                createLeagueViewController.title = "Create League"
                navigator.pushViewController(createLeagueViewController, animated: true)
            }
        }
    }
    
    @objc func addTeam(sender: UIBarButtonItem) {
        if let createTeamViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTeamViewController") as? CreateTeamViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                createTeamViewController.title = "Create Team"
                navigator.pushViewController(createTeamViewController, animated: true)
            }
        }
    }
    
    @objc func openSearch(sender: UIBarButtonItem) {
        if let searchViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                searchViewController.title = "Search"
                searchViewController.selected_type = "User"
                navigator.pushViewController(searchViewController, animated: true)
            }
        }
    }
    
    @objc func openSearchTeam(sender: UIBarButtonItem) {
        if let searchTeamViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                searchTeamViewController.title = "Search Team"
                searchTeamViewController.selected_type = "Team"
                navigator.pushViewController(searchTeamViewController, animated: true)
            }
        }
    }
    
    @objc func openSearchLeague(sender: UIBarButtonItem) {
        if let searchLeagueViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchLeagueViewController") as? SearchViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                searchLeagueViewController.title = "Search League"
                searchLeagueViewController.selected_type = "League"
                navigator.pushViewController(searchLeagueViewController, animated: true)
            }
        }
    }
    
    @objc func openNotification(sender: UIBarButtonItem) {
        if let notificationViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                notificationViewController.title = "Notifications"
                navigator.pushViewController(notificationViewController, animated: true)
            }
        }
    }
    
    @objc func openUpdateProfile(sender: UIBarButtonItem) {
        if let updateProfileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdateProfileViewController") as? UpdateProfileViewController {
            if let navigator = navigationController {
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                updateProfileViewController.title = "Profile Update"
                navigator.pushViewController(updateProfileViewController, animated: true)
            }
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
