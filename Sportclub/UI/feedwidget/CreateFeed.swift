//
//  CreateFeed.swift
//  Sportclub
//
//  Created by Hasan Abbas on 01/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class CreateFeed: UIView {
    
    weak var delegate:communicateDelegate?
    
    @IBOutlet weak var upic: UIImageView!
    
    @IBOutlet weak var statustext: UITextView!
    
    @IBOutlet weak var postimage: UIImageView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInitialization()
        // Setup view from .xib file
        // xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInitialization()
        // Setup view from .xib file
        //xibSetup()
    }
    
    func commonInitialization()
    {
        let view = Bundle.main.loadNibNamed("CreateFeednib", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        
        var borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        statustext.layer.borderWidth = 0.5
        statustext.layer.borderColor = borderColor.cgColor
        statustext.layer.cornerRadius = 5.0
        
    }
    
    @IBAction func send(_ sender: Any) {
        
        let DataToTransfer : [String:Any] =  ["Status": statustext.text , "Postimage": postimage.image  ]
        delegate?.tapbtn!(sender: sender, Cell: nil, identifier: "sendfeed", Data: DataToTransfer)
    }
    
    @IBAction func addphoto(_ sender: Any) {
        delegate?.tapbtn!(sender: sender, Cell: nil, identifier: "addphoto", Data: nil)
    }
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
