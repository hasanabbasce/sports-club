//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Team {
    
    var tid: String?
    var uid: String?
    var team_pic: String?
    var team_cover_pic: String?
    var team_maanger_status: String?
    var co_manager_name: String?
    var establishment_name: String?
    var team_name: String?
    var about: String?
    var sports: String?
    var level_of_play: String?
    var payment_method: String?
    var gallery = [Gallery]()
    var league : League?
    var players = [Useri]()
    
    var ref: DatabaseReference?
    var key: String = ""
    
    
    var location: String?
    
    var lat: Double?
    var lon: Double?
    var locality : String?
    var postalCode : String?
    var administrativeArea : String?
    var country : String?
    var countrycode : String?
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.tid = (snapshot.value as! NSDictionary)["tid"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.team_pic = (snapshot.value as! NSDictionary)["team_pic"] as? String ?? ""
        self.team_cover_pic = (snapshot.value as! NSDictionary)["team_cover_pic"] as? String ?? ""
        self.team_maanger_status = (snapshot.value as! NSDictionary)["team_maanger_status"] as? String ?? ""
        self.co_manager_name = (snapshot.value as! NSDictionary)["co_manager_name"] as? String ?? ""
        self.establishment_name = (snapshot.value as! NSDictionary)["establishment_name"] as? String ?? ""
        self.team_name = (snapshot.value as! NSDictionary)["team_name"] as? String ?? ""
        self.about = (snapshot.value as! NSDictionary)["about"] as? String ?? ""
        self.sports = (snapshot.value as! NSDictionary)["sports"] as? String ?? ""
        self.level_of_play = (snapshot.value as! NSDictionary)["level_of_play"] as? String ?? ""
        self.payment_method = (snapshot.value as! NSDictionary)["payment_method"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String ?? ""
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double ?? 0
        self.lon = (snapshot.value as! NSDictionary)["lon"] as? Double ?? 0
        self.locality = (snapshot.value as! NSDictionary)["locality"] as? String ?? ""
        self.postalCode = (snapshot.value as! NSDictionary)["postalCode"] as? String ?? ""
        self.administrativeArea = (snapshot.value as! NSDictionary)["administrativeArea"] as? String ?? ""
        self.country = (snapshot.value as! NSDictionary)["country"] as? String ?? ""
        self.countrycode = (snapshot.value as! NSDictionary)["countrycode"] as? String ?? ""
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["tid"] != nil)
        {
            self.tid = initdic["tid"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["team_pic"] != nil)
        {
            self.team_pic = initdic["team_pic"]! as? String
        }
        if(initdic["team_cover_pic"] != nil)
        {
            self.team_cover_pic = initdic["team_cover_pic"]! as? String
        }
        if(initdic["team_maanger_status"] != nil)
        {
            self.team_maanger_status = initdic["team_maanger_status"]! as? String
        }
        if(initdic["co_manager_name"] != nil)
        {
            self.co_manager_name = initdic["co_manager_name"]! as? String
        }
        if(initdic["establishment_name"] != nil)
        {
            self.establishment_name = initdic["establishment_name"]! as? String
        }
        if(initdic["team_name"] != nil)
        {
            self.team_name = initdic["team_name"]! as? String
        }
        if(initdic["about"] != nil)
        {
            self.about = initdic["about"]! as? String
        }
        if(initdic["sports"] != nil)
        {
            self.sports = initdic["sports"]! as? String
        }
        if(initdic["level_of_play"] != nil)
        {
            self.level_of_play = initdic["level_of_play"]! as? String
        }
        if(initdic["payment_method"] != nil)
        {
            self.payment_method = initdic["payment_method"]! as? String
        }
        
        
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["lon"] != nil)
        {
            self.lon = initdic["lon"]! as? Double
        }
        if(initdic["locality"] != nil)
        {
            self.locality = initdic["locality"]! as? String
        }
        if(initdic["postalCode"] != nil)
        {
            self.postalCode = initdic["postalCode"]! as? String
        }
        if(initdic["administrativeArea"] != nil)
        {
            self.administrativeArea = initdic["administrativeArea"]! as? String
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["countrycode"] != nil)
        {
            self.countrycode = initdic["countrycode"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.tid != nil)
        {
            initdic["tid"] = self.tid!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.tid!
        }
        if(self.team_pic != nil)
        {
            initdic["team_pic"] = self.team_pic!
        }
        if(self.team_cover_pic != nil)
        {
            initdic["team_cover_pic"] = self.team_cover_pic!
        }
        if(self.team_maanger_status != nil)
        {
            initdic["team_maanger_status"] = self.team_maanger_status!
        }
        if(self.co_manager_name != nil)
        {
            initdic["co_manager_name"] = self.co_manager_name!
        }
        if(self.establishment_name != nil)
        {
            initdic["establishment_name"] = self.establishment_name!
        }
        if(self.team_name != nil)
        {
            initdic["team_name"] = self.team_name!
        }
        if(self.about != nil)
        {
            initdic["about"] = self.about!
        }
        if(self.sports != nil)
        {
            initdic["sports"] = self.sports!
        }
        if(self.level_of_play != nil)
        {
            initdic["level_of_play"] = self.level_of_play!
        }
        if(self.payment_method != nil)
        {
            initdic["payment_method"] = self.payment_method!
        }
        
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        if(self.lon != nil)
        {
            initdic["lon"] = self.lon!
        }
        
        if(self.locality != nil)
        {
            initdic["locality"] = self.locality!
        }
        
        if(self.postalCode != nil)
        {
            initdic["postalCode"] = self.postalCode!
        }
        if(self.administrativeArea != nil)
        {
            initdic["administrativeArea"] = self.administrativeArea!
        }
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.countrycode != nil)
        {
            initdic["countrycode"] = self.countrycode!
        }
        
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}



