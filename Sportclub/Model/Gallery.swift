//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Gallery {
    
    var gid: String?
    var gallery_entity_type: String?
    var gallery_entity_id: String?
    var gallery_pic: String?
    
    var ref: DatabaseReference?
    var key: String = ""
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.gid = (snapshot.value as! NSDictionary)["gid"] as? String ?? ""
        self.gallery_entity_type = (snapshot.value as! NSDictionary)["gallery_entity_type"] as? String ?? ""
        self.gallery_entity_id = (snapshot.value as! NSDictionary)["gallery_entity_id"] as? String ?? ""
        self.gallery_pic = (snapshot.value as! NSDictionary)["gallery_pic"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
      
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["gid"] != nil)
        {
            self.gid = initdic["gid"]! as? String
        }
        if(initdic["gallery_entity_type"] != nil)
        {
            self.gallery_entity_type = initdic["gallery_entity_type"]! as? String
        }
        if(initdic["gallery_entity_id"] != nil)
        {
            self.gallery_entity_id = initdic["gallery_entity_id"]! as? String
        }
        if(initdic["gallery_pic"] != nil)
        {
            self.gallery_pic = initdic["gallery_pic"]! as? String
        }
        
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.gid != nil)
        {
            initdic["gid"] = self.gid!
        }
        if(self.gallery_entity_type != nil)
        {
            initdic["gallery_entity_type"] = self.gallery_entity_type!
        }
        if(self.gallery_entity_id != nil)
        {
            initdic["gallery_entity_id"] = self.gallery_entity_id!
        }
        if(self.gallery_pic != nil)
        {
            initdic["gallery_pic"] = self.gallery_pic!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}




