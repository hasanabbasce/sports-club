//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct League {
    
    var lid: String?
    var uid: String?
    var organization_name: String?
    var tournament_name: String?
    var about: String?
    var sports: String?
    var level_of_play: String?
    var start_date: String?
    var end_date: String?
    var day_of_week: String?
    var game_time: String?
    var game_per_season: String?
    var gender: String?
    var average_player_age: String?
    var player_per_team_max: String?
    var rules_and_regulations: String?
    var payment_method: String?
    var gallery = [Gallery]()
    var teams = [Team]()
    
    var ref: DatabaseReference?
    var key: String = ""
    
    
    var location: String?
    
    var lat: Double?
    var lon: Double?
    var locality : String?
    var postalCode : String?
    var administrativeArea : String?
    var country : String?
    var countrycode : String?
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.lid = (snapshot.value as! NSDictionary)["lid"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
         self.organization_name = (snapshot.value as! NSDictionary)["organization_name"] as? String ?? ""
         self.tournament_name = (snapshot.value as! NSDictionary)["tournament_name"] as? String ?? ""
         self.about = (snapshot.value as! NSDictionary)["about"] as? String ?? ""
         self.sports = (snapshot.value as! NSDictionary)["sports"] as? String ?? ""
         self.level_of_play = (snapshot.value as! NSDictionary)["level_of_play"] as? String ?? ""
         self.start_date = (snapshot.value as! NSDictionary)["start_date"] as? String ?? ""
         self.end_date = (snapshot.value as! NSDictionary)["end_date"] as? String ?? ""
        self.day_of_week = (snapshot.value as! NSDictionary)["day_of_week"] as? String ?? ""
        self.game_time = (snapshot.value as! NSDictionary)["game_time"] as? String ?? ""
        self.game_per_season = (snapshot.value as! NSDictionary)["game_per_season"] as? String ?? ""
        self.gender = (snapshot.value as! NSDictionary)["gender"] as? String ?? ""
        self.average_player_age = (snapshot.value as! NSDictionary)["average_player_age"] as? String ?? ""
        self.player_per_team_max = (snapshot.value as! NSDictionary)["player_per_team_max"] as? String ?? ""
        self.rules_and_regulations = (snapshot.value as! NSDictionary)["rules_and_regulations"] as? String ?? ""
        self.payment_method = (snapshot.value as! NSDictionary)["payment_method"] as? String ?? ""
        
        
        
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String ?? ""
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double ?? 0
        self.lon = (snapshot.value as! NSDictionary)["lon"] as? Double ?? 0
        self.locality = (snapshot.value as! NSDictionary)["locality"] as? String ?? ""
        self.postalCode = (snapshot.value as! NSDictionary)["postalCode"] as? String ?? ""
        self.administrativeArea = (snapshot.value as! NSDictionary)["administrativeArea"] as? String ?? ""
        self.country = (snapshot.value as! NSDictionary)["country"] as? String ?? ""
        self.countrycode = (snapshot.value as! NSDictionary)["countrycode"] as? String ?? ""
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["lid"] != nil)
        {
            self.lid = initdic["lid"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["organization_name"] != nil)
        {
            self.organization_name = initdic["organization_name"]! as? String
        }
        if(initdic["tournament_name"] != nil)
        {
            self.tournament_name = initdic["tournament_name"]! as? String
        }
        if(initdic["about"] != nil)
        {
            self.about = initdic["about"]! as? String
        }
        if(initdic["sports"] != nil)
        {
            self.sports = initdic["sports"]! as? String
        }
        if(initdic["level_of_play"] != nil)
        {
            self.level_of_play = initdic["level_of_play"]! as? String
        }
        if(initdic["start_date"] != nil)
        {
            self.start_date = initdic["start_date"]! as? String
        }
        if(initdic["end_date"] != nil)
        {
            self.end_date = initdic["end_date"]! as? String
        }
        if(initdic["day_of_week"] != nil)
        {
            self.day_of_week = initdic["day_of_week"]! as? String
        }
        if(initdic["game_time"] != nil)
        {
            self.game_time = initdic["game_time"]! as? String
        }
        if(initdic["game_per_season"] != nil)
        {
            self.game_per_season = initdic["game_per_season"]! as? String
        }
        if(initdic["gender"] != nil)
        {
            self.gender = initdic["gender"]! as? String
        }
        if(initdic["average_player_age"] != nil)
        {
            self.average_player_age = initdic["average_player_age"]! as? String
        }
        if(initdic["player_per_team_max"] != nil)
        {
            self.gender = initdic["player_per_team_max"]! as? String
        }
        if(initdic["rules_and_regulations"] != nil)
        {
            self.rules_and_regulations = initdic["rules_and_regulations"]! as? String
        }
        if(initdic["payment_method"] != nil)
        {
            self.payment_method = initdic["payment_method"]! as? String
        }
        
        
        
        
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["lon"] != nil)
        {
            self.lon = initdic["lon"]! as? Double
        }
        if(initdic["locality"] != nil)
        {
            self.locality = initdic["locality"]! as? String
        }
        if(initdic["postalCode"] != nil)
        {
            self.postalCode = initdic["postalCode"]! as? String
        }
        if(initdic["administrativeArea"] != nil)
        {
            self.administrativeArea = initdic["administrativeArea"]! as? String
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["countrycode"] != nil)
        {
            self.countrycode = initdic["countrycode"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.lid != nil)
        {
            initdic["lid"] = self.lid!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.organization_name != nil)
        {
            initdic["organization_name"] = self.organization_name!
        }
        if(self.tournament_name != nil)
        {
            initdic["tournament_name"] = self.tournament_name!
        }
        if(self.about != nil)
        {
            initdic["about"] = self.about!
        }
        if(self.sports != nil)
        {
            initdic["sports"] = self.sports!
        }
        if(self.level_of_play != nil)
        {
            initdic["level_of_play"] = self.level_of_play!
        }
        if(self.start_date != nil)
        {
            initdic["start_date"] = self.start_date!
        }
        if(self.start_date != nil)
        {
            initdic["start_date"] = self.start_date!
        }
        if(self.end_date != nil)
        {
            initdic["end_date"] = self.end_date!
        }
        if(self.day_of_week != nil)
        {
            initdic["day_of_week"] = self.day_of_week!
        }
        if(self.game_time != nil)
        {
            initdic["game_time"] = self.game_time!
        }
        if(self.game_per_season != nil)
        {
            initdic["game_per_season"] = self.game_per_season!
        }
        if(self.gender != nil)
        {
            initdic["gender"] = self.gender!
        }
        if(self.average_player_age != nil)
        {
            initdic["average_player_age"] = self.average_player_age!
        }
        if(self.player_per_team_max != nil)
        {
            initdic["player_per_team_max"] = self.player_per_team_max!
        }
        if(self.rules_and_regulations != nil)
        {
            initdic["rules_and_regulations"] = self.rules_and_regulations!
        }
        if(self.payment_method != nil)
        {
            initdic["payment_method"] = self.payment_method!
        }
        
        
        
        
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        if(self.lon != nil)
        {
            initdic["lon"] = self.lon!
        }
        
        if(self.locality != nil)
        {
            initdic["locality"] = self.locality!
        }
        
        if(self.postalCode != nil)
        {
            initdic["postalCode"] = self.postalCode!
        }
        if(self.administrativeArea != nil)
        {
            initdic["administrativeArea"] = self.administrativeArea!
        }
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.countrycode != nil)
        {
            initdic["countrycode"] = self.countrycode!
        }
        
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}


