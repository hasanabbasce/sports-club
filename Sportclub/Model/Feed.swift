//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Feed {
    
    var fid: String?
    var feed_type: String?
    var uid: String?
    var feed_text: String?
    var gallery = [Gallery]()
    
    var ref: DatabaseReference?
    var key: String = ""
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        self.fid = (snapshot.value as! NSDictionary)["fid"] as? String ?? ""
        self.feed_type = (snapshot.value as! NSDictionary)["feed_type"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.feed_text = (snapshot.value as! NSDictionary)["feed_text"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["fid"] != nil)
        {
            self.fid = initdic["fid"]! as? String
        }
        if(initdic["feed_type"] != nil)
        {
            self.feed_type = initdic["feed_type"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["feed_text"] != nil)
        {
            self.feed_text = initdic["feed_text"]! as? String
        }
        
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.fid != nil)
        {
            initdic["fid"] = self.fid!
        }
        if(self.feed_type != nil)
        {
            initdic["feed_type"] = self.feed_type!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.feed_text != nil)
        {
            initdic["feed_text"] = self.feed_text!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}



