//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct JoinedLeague {
    
    var jlid: String?
    var lid: String?
    var uid: String?
    var jlstatus: String?
    
    var ref: DatabaseReference?
    var key: String = ""
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.jlid = (snapshot.value as! NSDictionary)["jlid"] as? String ?? ""
        self.lid = (snapshot.value as! NSDictionary)["lid"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.jlstatus = (snapshot.value as! NSDictionary)["jlstatus"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["jlid"] != nil)
        {
            self.jlid = initdic["jlid"]! as? String
        }
        if(initdic["lid"] != nil)
        {
            self.lid = initdic["lid"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["jlstatus"] != nil)
        {
            self.jlstatus = initdic["jlstatus"]! as? String
        }
        
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.jlid != nil)
        {
            initdic["jlid"] = self.jlid!
        }
        if(self.lid != nil)
        {
            initdic["lid"] = self.lid!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.jlstatus != nil)
        {
            initdic["jlstatus"] = self.jlstatus!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}





