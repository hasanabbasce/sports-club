//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Useri {
    
    var uid: String?
    var email: String?
    var fullname: String?
    var profilePictureUrl: String?
    var coverPictureUrl: String?
    var gender: String?
    var sports: String?
    var levelofplay: String?
    var tshirtsize: String?
    var tshirtno: String?
    var about: String?
    var employer: String?
    var gallery = [Gallery]()
    var createdteam = [Team]()
    var joinedteam = [Team]()
    var createdleague = [Team]()
    var joinedleague = [Team]()
    
    var ref: DatabaseReference?
    var key: String = ""
    
    
    var location: String?
    var lat: Double?
    var lon: Double?
    var locality : String?
    var postalCode : String?
    var administrativeArea : String?
    var country : String?
    var countrycode : String?
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
       
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.email = (snapshot.value as! NSDictionary)["email"] as? String ?? ""
        self.fullname = (snapshot.value as! NSDictionary)["fullname"] as? String ?? ""
        self.profilePictureUrl = (snapshot.value as! NSDictionary)["profilePictureUrl"] as? String ?? ""
        self.coverPictureUrl = (snapshot.value as! NSDictionary)["coverPictureUrl"] as? String ?? ""
        self.gender = (snapshot.value as! NSDictionary)["gender"] as? String ?? ""
        
        self.sports = (snapshot.value as! NSDictionary)["sports"] as? String ?? ""
        self.levelofplay = (snapshot.value as! NSDictionary)["levelofplay"] as? String ?? ""
        self.tshirtsize = (snapshot.value as! NSDictionary)["tshirtsize"] as? String ?? ""
        self.tshirtno = (snapshot.value as! NSDictionary)["tshirtno"] as? String ?? ""
        self.about = (snapshot.value as! NSDictionary)["about"] as? String ?? ""
        self.employer = (snapshot.value as! NSDictionary)["employer"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String ?? ""
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double ?? 0
        self.lon = (snapshot.value as! NSDictionary)["lon"] as? Double ?? 0
        self.locality = (snapshot.value as! NSDictionary)["locality"] as? String ?? ""
        self.postalCode = (snapshot.value as! NSDictionary)["postalCode"] as? String ?? ""
        self.administrativeArea = (snapshot.value as! NSDictionary)["administrativeArea"] as? String ?? ""
        self.country = (snapshot.value as! NSDictionary)["country"] as? String ?? ""
        self.countrycode = (snapshot.value as! NSDictionary)["countrycode"] as? String ?? ""
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
   
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["email"] != nil)
        {
            self.email = initdic["email"]! as? String
        }
        if(initdic["fullname"] != nil)
        {
            self.fullname = initdic["fullname"]! as? String
        }
        if(initdic["profilePictureUrl"] != nil)
        {
            self.profilePictureUrl = initdic["profilePictureUrl"]! as? String
        }
        if(initdic["coverPictureUrl"] != nil)
        {
            self.coverPictureUrl = initdic["coverPictureUrl"]! as? String
        }
        if(initdic["gender"] != nil)
        {
            self.gender = initdic["gender"]!  as? String
        }
        if(initdic["sports"] != nil)
        {
            self.sports = initdic["sports"]!  as? String
        }
        if(initdic["levelofplay"] != nil)
        {
            self.levelofplay = initdic["levelofplay"]!  as? String
        }
        if(initdic["tshirtsize"] != nil)
        {
            self.tshirtsize = initdic["tshirtsize"]!  as? String
        }
        if(initdic["tshirtno"] != nil)
        {
            self.tshirtno = initdic["tshirtno"]!  as? String
        }
        if(initdic["about"] != nil)
        {
            self.about = initdic["about"]!  as? String
        }
        if(initdic["employer"] != nil)
        {
            self.employer = initdic["employer"]!  as? String
        }
        
        
        
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["lon"] != nil)
        {
            self.lon = initdic["lon"]! as? Double
        }
        if(initdic["locality"] != nil)
        {
            self.locality = initdic["locality"]! as? String
        }
        if(initdic["postalCode"] != nil)
        {
            self.postalCode = initdic["postalCode"]! as? String
        }
        if(initdic["administrativeArea"] != nil)
        {
            self.administrativeArea = initdic["administrativeArea"]! as? String
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["countrycode"] != nil)
        {
            self.countrycode = initdic["countrycode"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.email != nil)
        {
            initdic["email"] = self.email!
        }
        if(self.fullname != nil)
        {
            initdic["fullname"] = self.fullname!.lowercased()
            
        }
        if(self.profilePictureUrl != nil)
        {
            initdic["profilePictureUrl"] = self.profilePictureUrl!
        }
        if(self.gender != nil)
        {
            initdic["gender"] = self.gender!
        }
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        if(self.lon != nil)
        {
            initdic["lon"] = self.lon!
        }
        
        if(self.locality != nil)
        {
            initdic["locality"] = self.locality!
        }
        
        if(self.postalCode != nil)
        {
            initdic["postalCode"] = self.postalCode!
        }
        if(self.administrativeArea != nil)
        {
            initdic["administrativeArea"] = self.administrativeArea!
        }
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.countrycode != nil)
        {
            initdic["countrycode"] = self.countrycode!
        }
        
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
}
}
